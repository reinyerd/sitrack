package com.sitrack.pojo;

public class ObjetoReporte {

    private String fecha;
    private String patente;
    private double resultado;

    public ObjetoReporte(String fecha, String patente, double resultado) {
        this.fecha = fecha;
        this.patente = patente;
        this.resultado = resultado;
    }
    public String getPatente() {
        return patente;
    }
    public void setPatente(String patente) {
        this.patente = patente;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public double getResultado() {
        return resultado;
    }
    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
}
