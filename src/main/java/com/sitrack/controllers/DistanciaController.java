package com.sitrack.controllers;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sitrack.models.services.IReporteService;

@RestController
@RequestMapping("/api")
public class DistanciaController {

    private static final Logger LOGGER = Logger.getLogger(DistanciaController.class.getName());

    IReporteService reporteService;

    @Autowired
    DistanciaController(IReporteService reporteService){
        this.reporteService = reporteService;
    }

    @GetMapping("/distancia")
    public ResponseEntity<?> getDistance(@RequestParam String date, @RequestParam(required = false) String patente) {
        try {
            if(date != null && !date.equals("")){
                if(patente != null && !patente.equals("")){
                    return ResponseEntity.ok(reporteService.resultadoPorFechaPatente(date, patente));
                }
                else{
                    return ResponseEntity.ok(reporteService.resultadoPorFecha(date));
                }
            }
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
        return ResponseEntity.badRequest().build();
    }
}
