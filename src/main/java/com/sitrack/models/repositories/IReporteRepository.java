package com.sitrack.models.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sitrack.models.entities.Reporte;
import com.sitrack.pojo.ObjetoReporte;

@Repository
public interface IReporteRepository extends JpaRepository<Reporte, Long> {

    @Query("SELECT NEW com.sitrack.pojo.ObjetoReporte(:fecha , r.patente, MAX(r.odometro) - MIN(r.odometro)) " +
       "FROM Reporte r " +
       "WHERE r.fecha >= TO_TIMESTAMP(:fecha || ' 00:00:00', 'YYYY-MM-DD HH24:MI:SS') " +
       "AND r.fecha <= TO_TIMESTAMP(:fecha || ' 23:59:59', 'YYYY-MM-DD HH24:MI:SS') " +
       "GROUP BY r.patente")
    List<ObjetoReporte> resultadoPorFecha(@Param("fecha") String fecha);

    @Query("SELECT NEW com.sitrack.pojo.ObjetoReporte(:fecha , r.patente, MAX(r.odometro) - MIN(r.odometro)) " +
       "FROM Reporte r " +
       "WHERE r.fecha >= TO_TIMESTAMP(:fecha || ' 00:00:00', 'YYYY-MM-DD HH24:MI:SS') " +
       "AND r.fecha <= TO_TIMESTAMP(:fecha || ' 23:59:59', 'YYYY-MM-DD HH24:MI:SS') " +
       "AND r.patente = :patente " +
       "GROUP BY r.patente")
    List<ObjetoReporte> resultadoPorFechaPatente(@Param("fecha") String fecha, @Param("patente") String patente);
}
