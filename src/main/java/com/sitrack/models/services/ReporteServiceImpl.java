package com.sitrack.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sitrack.models.repositories.IReporteRepository;
import com.sitrack.pojo.ObjetoReporte;

@Service
public class ReporteServiceImpl implements IReporteService{

    IReporteRepository reporteRepository;

    @Autowired
    ReporteServiceImpl(IReporteRepository reporteRepository){
        this.reporteRepository = reporteRepository;
    }

    @Override
    public List<ObjetoReporte> resultadoPorFecha(String date) {
        return reporteRepository.resultadoPorFecha(date);
    }

    @Override
    public List<ObjetoReporte> resultadoPorFechaPatente(String date, String patente) {
        return reporteRepository.resultadoPorFechaPatente(date, patente);
    }

}
