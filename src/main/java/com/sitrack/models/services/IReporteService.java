package com.sitrack.models.services;

import java.util.List;

import com.sitrack.pojo.ObjetoReporte;

public interface IReporteService {

    List<ObjetoReporte> resultadoPorFecha(String date);

    List<ObjetoReporte> resultadoPorFechaPatente(String date, String patente);
}
