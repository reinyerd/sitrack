package com.sitrack.models.entities;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name = "reportes", schema = "public")
public class Reporte {

    @Id
    private Long id;
    private String patente;
    private LocalDateTime fecha;
    private double odometro;

    public String getPatente() {
        return patente;
    }
    public void setPatente(String patente) {
        this.patente = patente;
    }
    public LocalDateTime getFecha() {
        return fecha;
    }
    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }
    public double getOdometro() {
        return odometro;
    }
    public void setOdometro(double odometro) {
        this.odometro = odometro;
    }
}
