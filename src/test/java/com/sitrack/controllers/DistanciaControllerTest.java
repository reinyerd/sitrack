package com.sitrack.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.sitrack.models.services.IReporteService;
import com.sitrack.pojo.ObjetoReporte;

public class DistanciaControllerTest {

    private DistanciaController distanciaController;
    private IReporteService reporteService;

    @BeforeEach
    public void setUp() {
        reporteService = mock(IReporteService.class);
        distanciaController = new DistanciaController(reporteService);
    }

    @Test
    public void testGetDistanciaConFechaPatente() {
        String date = "2024-05-16";
        String patente = "AABB11";
        List<ObjetoReporte> resultadoEsperado = new ArrayList<>();
        resultadoEsperado.add(new ObjetoReporte("2024-05-16", "AABB11", 450.60297));
        when(reporteService.resultadoPorFechaPatente(date, patente)).thenReturn(resultadoEsperado);

        ResponseEntity<?> response = distanciaController.getDistance(date, patente);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(resultadoEsperado, response.getBody());
    }

    @Test
    public void testGetDistanciaConFecha() {
        String date = "2024-05-16";
        List<ObjetoReporte> resultadoEsperado = new ArrayList<>();
        resultadoEsperado.add(new ObjetoReporte("2024-05-16", "AABB11", 450.60297));
        resultadoEsperado.add(new ObjetoReporte("2024-05-16", "CCDD22", 925.4031));
        resultadoEsperado.add(new ObjetoReporte("2024-05-16", "EEFF33", 988.40295));
        when(reporteService.resultadoPorFecha(date)).thenReturn(resultadoEsperado);

        ResponseEntity<?> response = distanciaController.getDistance(date, null);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(resultadoEsperado, response.getBody());
    }
}
