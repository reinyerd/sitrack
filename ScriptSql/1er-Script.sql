-- Primero se corre este script que crea la base de datos
CREATE DATABASE db_sitrack
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;