--Esto se ejecuta luego de haber creado la base de datos sino no funcionara
CREATE TABLE IF NOT EXISTS "db_sitrack"."public"."reportes" (
  "id" bigserial,
  "patente" text COLLATE "pg_catalog"."default",
  "fecha" timestamptz(6),
  "odometro" float4,
  CONSTRAINT "reportes_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."reportes" 
  OWNER TO "postgres";

--Luego se ejecuta todos estos insert
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 09:46:31-04', '100.33');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 10:46:31-04', '150.13');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 14:46:31-04', '350.533');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 12:46:31-04', '250.353');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 13:46:31-04', '300.433');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 15:46:31-04', '400.633');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 17:46:31-04', '500.833');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 11:46:31-04', '200.233');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 18:46:31-04', '550.933');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-16 16:46:31-04', '450.733');

INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 09:46:31-04', '550.33');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 10:46:31-04', '600.13');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 17:46:31-04', '559.833');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 11:46:31-04', '588.233');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 14:46:31-04', '777.533');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 15:46:31-04', '667.633');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 18:46:31-04', '1880.68');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 16:46:31-04', '965.733');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 12:46:31-04', '888.353');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('AABB11', '2024-05-17 13:46:31-04', '999.433');

INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 09:46:31-04', '489.33');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 10:46:31-04', '600.13');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 13:46:31-04', '999.433');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 14:46:31-04', '1010.633');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 12:46:31-04', '888.353');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 15:46:31-04', '1111.833');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 18:46:31-04', '1414.733');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 17:46:31-04', '1313.58');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 16:46:31-04', '1212.233');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-16 11:46:31-04', '777.533');

INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 11:46:31-04', '1616.533');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 12:46:31-04', '1717.353');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 09:46:31-04', '1415.33');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 10:46:31-04', '1515.13');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 13:46:31-04', '1818.433');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 14:46:31-04', '1919.633');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 17:46:31-04', '2222.58');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 18:46:31-04', '2323.733');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 15:46:31-04', '2020.833');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('CCDD22', '2024-05-17 16:46:31-04', '2121.233');

INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 14:46:31-04', '555.633');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 15:46:31-04', '666.833');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 09:46:31-04', '11.33');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 10:46:31-04', '111.13');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 16:46:31-04', '777.233');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 11:46:31-04', '222.533');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 12:46:31-04', '333.353');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 13:46:31-04', '444.433');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 17:46:31-04', '888.58');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-16 18:46:31-04', '999.733');

INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 10:46:31-04', '1010.13');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 11:46:31-04', '1111.533');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 12:46:31-04', '1212.353');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 17:46:31-04', '1717.58');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 18:46:31-04', '1818.733');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 13:46:31-04', '1313.433');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 14:46:31-04', '1414.633');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 15:46:31-04', '1515.833');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 16:46:31-04', '1616.233');
INSERT INTO "public"."reportes"("patente", "fecha", "odometro") VALUES ('EEFF33', '2024-05-17 09:46:31-04', '999.33');