¡Bienvenidos a la solución de mi prueba! A continuación, se detallan los pasos necesarios para desplegar el proyecto correctamente.

Pasos para desplegar el proyecto

1. Descargar archivos necesarios
   Descargue los archivos desde el siguiente enlace de Google Drive, donde encontrará la versión de Java 19 y la versión de Maven que utilicé para las dependencias y empaquetado del proyecto:
   Enlace de Google Drive

https://drive.google.com/drive/folders/1YajQg2IUyJpjDbfAc7LbWDn88i9cmR0j?usp=sharing

2. Configurar variables de entorno
   Configure tanto Maven como Java 19 en las variables de entorno. Para ello, siga estos pasos:

Acceda a las variables de entorno del sistema.
Diríjase a la variable Path y agregue los directorios donde haya instalado Maven y Java. Asegúrese de incluir la carpeta bin de cada uno.

3. Verificar la instalación
   Para comprobar que el sistema detectó correctamente Maven y Java, abra la consola y ejecute los siguientes comandos:

java --version
mvn --version
Verificación de instalación

![alt text](image-1.png)

4. Crear base de datos
   Acceda al directorio donde clonó el proyecto desde Git, llamado sitrack.
   Ingrese a la carpeta ScriptSql y ejecute el archivo 1er-Script.sql para crear una nueva base de datos en PostgreSQL llamada db_sitrack.

   Conéctese a la base de datos creada y ejecute el segundo script contenido en el archivo 2do-Script.sql.

   Para que el proyecto se conecte correctamente a la base de datos en su equipo, hay un último paso que debe realizar. En mi equipo, tengo configurado PostgreSQL con la clave "admin". Sin embargo, en su equipo, PostgreSQL probablemente tendrá otras credenciales.

   Para que el proyecto pueda conectarse a la base de datos en su equipo o servidor, ya sea de manera local o en un entorno de pruebas, debe seguir estos pasos:

   Diríjase a la siguiente ruta dentro del proyecto: /sitrack/src/main/resources/application.properties.
   Abra el archivo application.properties.
   Busque la propiedad spring.datasource.password=admin.
   Reemplace admin con la clave que utiliza en su configuración de PostgreSQL.
   Es importante realizar esta modificación antes del paso 5, para que cuando empaquete y ejecute el proyecto, ya tenga este cambio aplicado.

5. Empaquetar el proyecto
   Desde la terminal, diríjase a la carpeta raíz del proyecto (sitrack).
   Ejecute el siguiente comando para limpiar y empaquetar el proyecto:

   mvn clean package
   Abra Visual Studio Code, y ejecute el proyecto utilizando la herramienta de Spring Boot instalada en VSCode.

6. Ejecución sin IDE (Opcional)
   Si no tiene instalado VSCode, Eclipse u otra herramienta, puede ejecutar el proyecto desde la terminal:

Muévase a la carpeta target generada después de ejecutar el comando mvn clean package.
Ejecute el siguiente comando para iniciar la aplicación:
java -jar sitrack-1.0.0.jar

7. Acceder al endpoint
   Acceda al endpoint a través de la siguiente URL:
   127.0.0.1:8080/api/distancia

Parámetros del endpoint:
URL Base: 127.0.0.1:8080/api/distancia
Parámetros:
date=2024-05-17
patente=AABB11 (para consultas por patente y fecha)
Información adicional
Las patentes cargadas en la base de datos por el script son:

Patente AABB11
Patente CCDD22
Patente EEFF33

Los días con registros son:
2024-05-16
2024-05-17

Notas finales
La prueba incluye todas las respuestas requeridas, junto con sus respectivas pruebas unitarias. Si el sistema presenta algún error o si necesita una explicación adicional sobre el código, estoy disponible para reunirme con ustedes nuevamente.

Sugerencias de mejora

- Incorporar parámetros de entrada fecha_desde y fecha_hasta para consultas más extensas.

- Dependiendo del motor de base de datos, optimizar los tiempos de entrega de datos realizando cálculos directamente en el motor. Para este desarrollo, realicé todos los cálculos en el motor de base de datos por considerarlo más óptimo y rápido.
  Quedo atento a sus comentarios.

Saludos,
Reinyerd Babinczuk
